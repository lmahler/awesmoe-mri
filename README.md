# Awesome MRI
___________
This shall serve as a central point to collect scripts, tools and everything related to the MRI domain

# Contents
___________

- HPC
    - [restart slurm job automatically after x hours](HPC/restart_slurm.sh)
    - [sync files to raven/cobra/nyx/eris using rsync](HPC/cluster_sync.sh)
    

# ToDo's
___________
- [ ] create contribution guidelines
    - [ ] Merge requests for every commit
    - [ ] show minimum examples for script
    - [ ] Structure of content
