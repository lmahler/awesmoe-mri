#!/bin/bash
# set your mpcdf username here
$USERNAME=example

# first argument is local path, second argument is remote path
rsync -aP -e 'ssh -J $USERNAME@gateafs.mpcdf.mpg.de' $1 $USERNAME@eris01.bc.rzg.mpg.de:$2

# usage:
# ./clustersync.sh ./local_folder /ptmp/example/dir/at/remote/cluster/
